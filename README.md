## Asyncfire
One more asynchronous event loop implementation (pure python). 
It's a training project and does't meant to be in production.
You can find here some examples how such event loops can be 
implemented using python async/await syntax. 
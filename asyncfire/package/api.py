import socket
from typing import Tuple

from asyncfire.package.core.socket import SocketAcceptAwaitable
from asyncfire.package.core.socket import SocketCloseAwaitable
from asyncfire.package.core.socket import SocketReadAwaitable
from asyncfire.package.core.loop.event_loop import EventLoop
from asyncfire.package.core.sleep import SleepAwaitable

__all__ = [
    'EventLoop',
    'sleep',
    'create_socket',
    'bind',
    'listen',
    'accept',
    'read',
    'close'
]


async def sleep(seconds: float):
    await SleepAwaitable(seconds)


def create_socket() -> socket.socket:
    sock = socket.socket()
    sock.setblocking(False)
    return sock


Address = Tuple[str, int]


def bind(sock: socket.socket, address: Address):
    sock.bind(address)


def listen(sock: socket.socket):
    sock.listen()


async def accept(sock: socket.socket) -> Tuple[socket.socket, Address]:
    return await SocketAcceptAwaitable(sock)


async def read(sock: socket.socket, n_bytes: int) -> bytes:
    return await SocketReadAwaitable(sock, n_bytes)


async def close(sock: socket.socket):
    await SocketCloseAwaitable(sock)

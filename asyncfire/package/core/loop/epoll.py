import enum
import select
from dataclasses import dataclass
from datetime import timedelta
from typing import Dict
from typing import List
from typing import Optional
from typing import Set


class FileOperation(enum.Enum):
    READ = enum.auto()
    WRITE = enum.auto()


@dataclass(frozen=True)
class EpollEvent:
    fileno: int
    operation: FileOperation


FILE_OPERATION_TO_EPOLL_FLAG = {
        FileOperation.READ: select.EPOLLIN,
        FileOperation.WRITE: select.EPOLLOUT
}


class EpollService:

    def __init__(self):
        self._epoll = select.epoll()
        self._registered_fileno_to_file_operations: Dict[int, Set[FileOperation]] = dict()

    def has_event(self, event: EpollEvent) -> bool:
        return event.fileno in self._registered_fileno_to_file_operations and \
               event.operation in self._registered_fileno_to_file_operations[event.fileno]

    def has_file_events(self, fileno: int) -> bool:
        return fileno in self._registered_fileno_to_file_operations

    def remove_event(self, event: EpollEvent):
        assert self.has_event(event)

        self._registered_fileno_to_file_operations[event.fileno].remove(event.operation)
        if not self._registered_fileno_to_file_operations[event.fileno]:
            del self._registered_fileno_to_file_operations[event.fileno]
            self._epoll.unregister(event.fileno)
        else:
            new_mask = eval_epoll_mask(self._registered_fileno_to_file_operations[event.fileno])
            self._epoll.modify(event.fileno, new_mask)

    def remove_file_events(self, fileno: int):
        assert fileno in self._registered_fileno_to_file_operations

        del self._registered_fileno_to_file_operations[fileno]
        self._epoll.unregister(fileno)

    def change_file_event(self, from_event: EpollEvent, to_event: EpollEvent):
        assert from_event.fileno == to_event.fileno
        assert self.has_event(from_event)

        if from_event.operation == to_event.operation:
            return

        new_mask = eval_epoll_mask({to_event.operation})
        self._epoll.modify(to_event.fileno, new_mask)

    def add_event(self, event: EpollEvent):
        if event.fileno not in self._registered_fileno_to_file_operations:
            file_operations = {event.operation}
            self._registered_fileno_to_file_operations[event.fileno] = file_operations
            mask = eval_epoll_mask(file_operations)
            self._epoll.register(event.fileno, mask)
        elif event.operation not in self._registered_fileno_to_file_operations[event.fileno]:
            self._registered_fileno_to_file_operations[event.fileno].add(event.operation)
            new_mask = eval_epoll_mask(self._registered_fileno_to_file_operations[event.fileno])
            self._epoll.modify(event.fileno, new_mask)

    def wait_for_next_ready_events(self, timeout: Optional[timedelta]) -> List[EpollEvent]:
        to_milliseconds = 1000
        timeout_ms = timeout.total_seconds() * to_milliseconds if timeout is not None else None

        epoll_result = self._epoll.poll(timeout_ms)

        ready_events = list()
        for fileno, flags in epoll_result:
            if flags & select.EPOLLIN:
                ready_events.append(EpollEvent(fileno, FileOperation.READ))
            if flags & select.EPOLLOUT:
                ready_events.append(EpollEvent(fileno, FileOperation.WRITE))

        return ready_events


def eval_epoll_mask(file_operations: Set[FileOperation]) -> int:
    mask = 0
    for file_operation in file_operations:
        mask |= FILE_OPERATION_TO_EPOLL_FLAG[file_operation]

    return mask

from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from datetime import timedelta
from typing import Any
from typing import Coroutine
from typing import List
from typing import Optional
from uuid import UUID

from asyncfire.package.core.loop.epoll import EpollEvent
from asyncfire.package.core.loop.epoll import EpollService
from asyncfire.package.core.loop.epoll import FileOperation
from asyncfire.package.core.loop.target_events import SocketOperation
from asyncfire.package.core.loop.target_events import SocketTargetEvent
from asyncfire.package.core.loop.target_events import TargetEvent
from asyncfire.package.core.loop.target_events import TimeTargetEvent
from asyncfire.package.core.loop.task_containers import CreatedTaskContainer
from asyncfire.package.core.loop.task_containers import EpollTaskContainer
from asyncfire.package.core.loop.task_containers import TimeTaskContainer
from asyncfire.package.core.loop.tasks import Task
from asyncfire.package.core.loop.tasks import TaskState
from asyncfire.package.core.loop.tasks import WaitingState


class EventLoop:

    def __init__(self):
        self._created_tasks = CreatedTaskContainer()
        self._time_tasks = TimeTaskContainer()
        self._epoll_tasks = EpollTaskContainer()

        self._epoll_service = EpollService()

    def add_task(self, coroutine: Coroutine[TargetEvent, None, Any]):
        task = Task(coroutine)
        self._created_tasks.add_task(task)

    def has_tasks(self) -> bool:
        return self._created_tasks.has_tasks() or self._time_tasks.has_tasks() or self._epoll_tasks.has_tasks()

    def run(self):
        while self.has_tasks():
            sleep_timeout = self._eval_sleep_timeout()
            ready_events = self._epoll_service.wait_for_next_ready_events(sleep_timeout)
            self._process_tasks(ready_events)

    def _process_tasks(self, ready_events: List[EpollEvent]):
        ready_tasks = [
            *self._time_tasks.pop_ready_tasks(),
            *self._created_tasks.pop_ready_tasks(),
            *self._epoll_tasks.pop_ready_tasks(ready_events)
        ]

        for task in ready_tasks:
            self._process_task(task)

    def _process_task(self, task: Task):
        prev_state = task.state
        task.step()

        self._handle_socket_close_state(task)
        self._handle_epoll_event_change(task, prev_state)
        self._add_task_to_container(task)

    def _handle_socket_close_state(self, task: Task):
        # all close on the same step: no blocking io
        while (
                isinstance(task.state, WaitingState) and
                isinstance(task.state.event, SocketTargetEvent) and
                task.state.event.operation == SocketOperation.CLOSE
        ):
            if self._epoll_service.has_file_events(task.state.event.fileno):
                self._epoll_service.remove_file_events(task.state.event.fileno)
            task.step()

    def _add_task_to_container(self, task: Task):
        socket_op_to_epoll_op = {
            SocketOperation.READ: FileOperation.READ,
            SocketOperation.WRITE: FileOperation.WRITE
        }

        cur_state = task.state
        if isinstance(cur_state, WaitingState) and isinstance(cur_state.event, TimeTargetEvent):
            self._time_tasks.add_task(task)
        elif (
                isinstance(cur_state, WaitingState) and
                isinstance(cur_state.event, SocketTargetEvent) and
                cur_state.event.operation != SocketOperation.CLOSE
        ):
            new_epoll_event = EpollEvent(
                cur_state.event.fileno,
                socket_op_to_epoll_op[cur_state.event.operation]
            )
            self._epoll_tasks.add_task(new_epoll_event, task)

    def _handle_epoll_event_change(self, task: Task, prev_state: TaskState):
        socket_op_to_epoll_op = {
            SocketOperation.READ: FileOperation.READ,
            SocketOperation.WRITE: FileOperation.WRITE
        }

        cur_state = task.state

        def waited_for_epoll_and_no_more(prev_state: TaskState, cur_state: TaskState) -> bool:
            prev_is_epoll = (
                    isinstance(prev_state, WaitingState) and
                    isinstance(prev_state.event, SocketTargetEvent) and
                    prev_state.event.operation != SocketOperation.CLOSE
            )
            cur_is_not_epoll = not (
                    isinstance(cur_state, WaitingState) and
                    isinstance(cur_state.event, SocketTargetEvent) and
                    cur_state.event.operation != SocketOperation.CLOSE
            )
            return prev_is_epoll and cur_is_not_epoll

        def epoll_events_changed(prev_state: TaskState, cur_state: TaskState) -> bool:
            return (
                    isinstance(prev_state, WaitingState) and
                    isinstance(prev_state.event, SocketTargetEvent) and
                    prev_state.event.operation != SocketOperation.CLOSE and
                    isinstance(cur_state, WaitingState) and
                    isinstance(cur_state.event, SocketTargetEvent) and
                    cur_state.event.operation != SocketOperation.CLOSE and
                    prev_state.event.operation != cur_state.event.operation
            )

        def is_new_epoll_event(prev_state: TaskState, cur_state: TaskState) -> bool:
            prev_is_not_epoll = not (
                    isinstance(prev_state, WaitingState) and
                    isinstance(prev_state.event, SocketTargetEvent) and
                    prev_state.event.operation != SocketOperation.CLOSE
            )
            cur_is_epoll = (
                    isinstance(cur_state, WaitingState) and
                    isinstance(cur_state.event, SocketTargetEvent) and
                    cur_state.event.operation != SocketOperation.CLOSE
            )
            return prev_is_not_epoll and cur_is_epoll

        if waited_for_epoll_and_no_more(prev_state, cur_state):
            old_epoll_event = EpollEvent(
                prev_state.event.fileno,
                socket_op_to_epoll_op[prev_state.event.operation]
            )
            if self._epoll_service.has_event(old_epoll_event):
                self._epoll_service.remove_event(old_epoll_event)

        elif epoll_events_changed(prev_state, cur_state):
            old_epoll_event = EpollEvent(
                prev_state.event.fileno,
                socket_op_to_epoll_op[prev_state.event.operation]
            )
            new_epoll_event = EpollEvent(
                cur_state.event.fileno,
                socket_op_to_epoll_op[cur_state.event.operation]
            )
            self._epoll_service.change_file_event(old_epoll_event, new_epoll_event)
            self._epoll_tasks.add_task(new_epoll_event, task)

        elif is_new_epoll_event(prev_state, cur_state):
            new_epoll_event = EpollEvent(
                cur_state.event.fileno,
                socket_op_to_epoll_op[cur_state.event.operation]
            )
            self._epoll_service.add_event(new_epoll_event)
            self._epoll_tasks.add_task(new_epoll_event, task)

    def _eval_sleep_timeout(self) -> Optional[timedelta]:
        if self._created_tasks.has_tasks():
            return timedelta(seconds=0)

        return self._time_tasks.earliest_task_timeout()

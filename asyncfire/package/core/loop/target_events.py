import enum
from dataclasses import dataclass
from datetime import datetime
from datetime import timedelta
from typing import Union


class SocketOperation(enum.Enum):
    READ = enum.auto()
    WRITE = enum.auto()
    CLOSE = enum.auto()


@dataclass
class SocketTargetEvent:
    fileno: int
    operation: SocketOperation


class TimeTargetEvent:
    def __init__(self, seconds: float):
        self.datetime = datetime.now() + timedelta(seconds=seconds)


TargetEvent = Union[
    TimeTargetEvent,
    SocketTargetEvent
]
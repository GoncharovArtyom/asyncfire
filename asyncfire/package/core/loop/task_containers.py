from dataclasses import dataclass
from datetime import datetime
from datetime import timedelta
from functools import total_ordering
from heapq import heappop
from heapq import heappush
from typing import List
from typing import Optional

from asyncfire.package.core.loop.epoll import EpollEvent
from asyncfire.package.core.loop.tasks import Task


@dataclass
@total_ordering
class HeapItem:
    task: Task

    def __le__(self, other: 'HeapItem') -> bool:
        return self.task.state.event.datetime < other.task.state.event.datetime

    def __eq__(self, other: 'HeapItem') -> bool:
        return self.task.state.event.datetime == other.task.state.event.datetime


class TimeTaskContainer:

    def __init__(self):
        self._time_task_heap: List[HeapItem] = []

    def add_task(self, task: Task):
        heappush(self._time_task_heap, HeapItem(task))

    def has_tasks(self) -> bool:
        return bool(self._time_task_heap)

    def earliest_task_timeout(self) -> Optional[timedelta]:
        if not self._time_task_heap:
            return None

        now = datetime.now()
        if self._time_task_heap[0].task.state.event.datetime <= now:
            return timedelta(seconds=0)

        return self._time_task_heap[0].task.state.event.datetime - now

    def pop_ready_tasks(self) -> List[Task]:
        ready_tasks = []
        now = datetime.now()

        while self._time_task_heap and self._time_task_heap[0].task.state.event.datetime <= now:
            heap_item = heappop(self._time_task_heap)
            ready_tasks.append(heap_item.task)

        return ready_tasks


class CreatedTaskContainer:

    def __init__(self):
        self._created_tasks: List[Task] = []

    def add_task(self, task: Task):
        self._created_tasks.append(task)

    def has_tasks(self) -> bool:
        return bool(self._created_tasks)

    def pop_ready_tasks(self) -> List[Task]:
        ready_tasks = self._created_tasks
        self._created_tasks = []
        return ready_tasks


class EpollTaskContainer:

    def __init__(self):
        self._event_to_epoll_task = dict()

    def add_task(self, event: EpollEvent, task: Task):
        self._event_to_epoll_task[event] = task

    def has_tasks(self) -> bool:
        return bool(self._event_to_epoll_task)

    def pop_ready_tasks(self, ready_events: List[EpollEvent]) -> List[EpollEvent]:
        ready_tasks = []
        for event in ready_events:
            ready_tasks.append(self._event_to_epoll_task.pop(event))

        return ready_tasks

from dataclasses import dataclass
from typing import Any
from typing import Coroutine
from typing import Union
from uuid import uuid4

from asyncfire.package.core.loop.target_events import TargetEvent


class CreatedState:
    pass


class FinishedState:
    pass


@dataclass
class WaitingState:
    event: TargetEvent


TaskState = Union[
    CreatedState,
    WaitingState,
    FinishedState
]


class Task:
    def __init__(self, coroutine: Coroutine[TargetEvent, None, Any]):
        self.uid = uuid4()
        self.state: TaskState = CreatedState()
        self.iterator = coroutine.__await__()

    def step(self):
        try:
            event = next(self.iterator)
            self.state = WaitingState(event)
        except StopIteration:
            self.state = FinishedState()

from typing import Generator

from asyncfire.package.core.loop.target_events import TimeTargetEvent


class SleepAwaitable:

    def __init__(self, seconds: float):
        self._seconds = seconds

    def __await__(self) -> Generator[TimeTargetEvent, None, None]:
        yield TimeTargetEvent(self._seconds)

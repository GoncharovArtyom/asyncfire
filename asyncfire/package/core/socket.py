import socket
from typing import Generator
from typing import Tuple

from asyncfire.package.core.loop.target_events import SocketOperation
from asyncfire.package.core.loop.target_events import SocketTargetEvent

Address = Tuple[str, int]
AcceptResult = Tuple[socket.socket, Address]


class SocketAcceptAwaitable:

    def __init__(self, sock: socket.socket):
        assert not sock.getblocking()
        self._sock = sock

    def __await__(self) -> Generator[SocketTargetEvent, None, AcceptResult]:
        yield SocketTargetEvent(self._sock.fileno(), SocketOperation.READ)
        new_sock, address = self._sock.accept()
        new_sock.setblocking(False)
        return new_sock, address


class SocketReadAwaitable:

    def __init__(self, sock: socket.socket, n_bytes: int):
        assert not sock.getblocking()
        self._socket = sock
        self._n_bytes = n_bytes

    def __await__(self) -> Generator[SocketTargetEvent, None, bytes]:
        yield SocketTargetEvent(self._socket.fileno(), SocketOperation.READ)
        read_bytes = self._socket.recv(self._n_bytes)
        return read_bytes


class SocketCloseAwaitable:

    def __init__(self, sock: socket.socket):
        assert not sock.getblocking()
        self._socket = sock

    def __await__(self) -> Generator[SocketTargetEvent, None, None]:
        yield SocketTargetEvent(self._socket.fileno(), SocketOperation.CLOSE)
        self._socket.close()

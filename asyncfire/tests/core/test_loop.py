import time

from asyncfire.package.api import sleep
from asyncfire.package.api import EventLoop


def test_event_loop_executes_usual_function():
    loop = EventLoop()
    was_called = False

    async def foo():
        nonlocal was_called
        was_called = True

    loop.add_task(foo())
    loop.run()

    assert was_called


def test_event_loop_executes_function_with_sleep():
    loop = EventLoop()
    was_called = False

    async def foo():
        await sleep(0.000001)
        nonlocal was_called
        was_called = True

    loop.add_task(foo())
    loop.run()

    assert was_called


def test_event_loop_sleeps_enough_seconds():
    loop = EventLoop()

    sleep_duration_s = 0.001
    real_sleep_duration_s = None

    async def foo():
        start_s = time.time()
        await sleep(sleep_duration_s)
        end_s = time.time()

        nonlocal real_sleep_duration_s
        real_sleep_duration_s = end_s - start_s

    loop.add_task(foo())
    loop.run()

    assert real_sleep_duration_s is not None
    assert real_sleep_duration_s >= sleep_duration_s


def test_event_loop_executes_2_functions():
    loop = EventLoop()
    was_called = [False, False]

    async def foo():
        nonlocal was_called
        was_called[0] = True

    async def bar():
        nonlocal was_called
        was_called[1] = True

    loop.add_task(foo())
    loop.add_task(bar())
    loop.run()

    assert all(was_called)
